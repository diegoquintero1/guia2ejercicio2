#include <iostream>

using namespace std;
//programa que genera 200 letras aleatorias y cuenta cuantas letras de cada tipo hay:
int main()
{char letras[26]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    int contadorLetra[26] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//lugar donde se van a guardar el numero de letras
    char letra;
    for (int i=0;i<200;i++){//ciclo que va dando una letra al azar hasta completar las 200
        letra=  letras[rand()%26];
        cout << letra;

        for (int i=0;i<26;i++)//ciclo que guarda las letras aleatorias ordenadas alfabeticamente para luego imprimir cunats letras de cierto tipo hay
            if (letra == letras[i]){
                contadorLetra[i]+=1;
                break;
            }
    }
    cout <<"\n";

        for (int i=0;i<26;i++){//ciclo que va imprimiendo la letra y el numero de veces que se repite al generar las 200 aleatorias
            cout << letras[i]<<": "<<contadorLetra[i]<<endl;
        }
}
